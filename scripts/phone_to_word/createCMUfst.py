#!/bin/python

# Create the CMUdict wfst 

# Input:	cmudict-0.7b
# Output:	cmudict.wfst

# To check:
# echo EASIER SAID THAN DONE | carmel -sliQOWEk 5 cmudict.fst 
# echo IY1 Z IY0 ER0 S EH1 D DH AE1 N D AH1 N | carmel -sriQIWEk 30 cmudict.fst

noStressSwitch = 1;  # switch on if u want a no-stress cmu WFST
import os
os.system("grep -a '^[A-Z]' ../../data/cmudict-0.7b | sed 's/(/foo---/g' | sed 's/)/---bar/g' | perl -pe 's|(foo.*?bar)||g' > tempfile")
# "-a" flag to grep was required since the dicitonary is treated as a binary file otherwise, leading to improper output

fid = open('tempfile','r')
data = fid.readlines()
fid.close()
words = [x.split()[0] for x in data]
numWords = len(words)
prons = [x.split()[1:] for x in data]
if noStressSwitch==1:			# Removing stress
	for i,x in enumerate(prons):
		s = ' '.join(x)
		s = ''.join([j for j in s if not j.isdigit()])
		prons[i] = s.split(' ')
	wfstFile = open('cmudict_nostress.fst','w')
else:
	wfstFile = open('cmudict.fst','w')
wfstFile.write('S0\n')

stateCounter=1
for wordI,wordV in enumerate(words):
	if len(prons[wordI])==1:
		wfstFile.write('(S0 (S0 '+str(wordV)+' '+str(' '.join(prons[wordI]))+' '+str(1/float(numWords))+'))\n')
	else:
		wfstFile.write('(S0 (S'+str(stateCounter)+' '+str(wordV)+' *e* '+str(1/float(numWords))+'))\n')
		for phoneI in range(0,len(prons[wordI])-1):
			wfstFile.write('(S'+str(stateCounter)+' (S'+str(stateCounter+1)+' *e* '+str(prons[wordI][phoneI])+'))\n')
			stateCounter+=1
		wfstFile.write('(S'+str(stateCounter)+' (S0 *e* '+str(prons[wordI][-1])+'))\n')
		stateCounter+=1

wfstFile.write('(S0 (S0 "_" "_"))\n')
wfstFile.close()
#os.system('rm tempfile')
