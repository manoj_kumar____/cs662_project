#!/bin/python
import os
# this script tests if a bad phone seq after passing thru phone.wfst and cmudict.wfst results in the good word seq among the top 10000 hypothesis

fid = open('../train_test_split/datasets/set_1/train.txt','r')
badPhones = fid.readlines()
fid.close()
badPhones = [x.strip('\n').split() for x in badPhones]
goodPhones = badPhones[0::3]
badPhones = badPhones[1::3]

fid = open('../../data/madgab.pairs','r')
#fid = open('small_madgab.pairs','r')
data = fid.readlines()
fid.close()
data = [x.strip('\n').split(' ') for x in data]
allGoodWords = data[0::5]
allGoodPhones = data[1::5]
allGoodPhones = [' '.join(x) for x in allGoodPhones]
bigDict = dict(zip(allGoodPhones,allGoodWords))

found,not_found=0,0
for bpi,bpv in enumerate(badPhones):
	os.system('echo '+' '.join(bpv)+' | carmel -sriQWIEk 10000 cmudict.fst ../train_test_split/datasets/set_1/phone.wfst > out')
	goldWordSeq = bigDict[' '.join(goodPhones[bpi])+' ']
	if (os.system('grep \"'+' '.join(goldWordSeq)+'\" out')):
		print("not found")
		not_found+= 1
	else:
		print("found")
		found+=1
