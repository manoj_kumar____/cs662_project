#!/bin/python
# Creates the confusion matrix for use in Levenshtein distance 
import csv

fid1 = open('mat1.txt','r')
input1 = fid1.readlines()
fid1.close()
del input1[0]
input1 = [x.strip('\n').upper().split() for x in input1]

fid2 = open('mat2.txt','r')
input2 = fid2.readlines()
fid2.close()
del input2[0]
input2 = [x.strip('\n').upper().split() for x in input2]

fid3 = open('mat3.txt','r')
input3 = fid3.readlines()
fid3.close()
del input3[0]
input3 = [x.strip('\n').upper().split() for x in input3]


fid4 = open('../cmuPhoneList','r')
cmuPhones = fid4.readlines()
fid4.close()
cmuPhones = [x.strip('\n') for x in cmuPhones]

simMatrix = [[0 for i in range(0,len(cmuPhones))] for j in range(0,len(cmuPhones))]

for i1,p1 in enumerate(cmuPhones):
	for i2,p2 in enumerate(cmuPhones):
		if p1==p2:
			simMatrix[i1][i2] = 1			
		else:
			numLocs = 0
			foundp1,foundp2,indexp2,indexp2 = 0,0,0,0
			for i,x in enumerate(input1):
				if x[0]==p1:
					foundp1 = 1
					indexp1 = i
				if x[0]==p2:
				 	foundp2 = 1
					indexp2 = i					
			if foundp1==1 and foundp2==1:
				numLocs += 1
				simMatrix[i1][i2] = float(input1[indexp1][indexp2+1])

			foundp1,foundp2,indexp2,indexp2 = 0,0,0,0
			for i,x in enumerate(input2):
				if x[0]==p1:
					foundp1 = 1
					indexp1 = i
				if x[0]==p2:
				 	foundp2 = 1
					indexp2 = i					
			if foundp1==1 and foundp2==1:
				numLocs += 1
				simMatrix[i1][i2] = float(input2[indexp1][indexp2+1])

			foundp1,foundp2,indexp2,indexp2 = 0,0,0,0
			for i,x in enumerate(input3):
				if x[0]==p1:
					foundp1 = 1
					indexp1 = i
				if x[0]==p2:
				 	foundp2 = 1
					indexp2 = i					
			if foundp1==1 and foundp2==1:
				numLocs += 1
				simMatrix[i1][i2] = float(input3[indexp1][indexp2+1])

			if numLocs:
				simMatrix[i1][i2] = simMatrix[i1][i2]/numLocs
			else:
				simMatrix[i1][i2] = 0

outfile = open('simMatrix.csv','w')
writer = csv.writer(outfile)
writer.writerows(simMatrix)
outfile.close()
