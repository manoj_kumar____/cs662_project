#!/bin/python

# Second version of our evaluations scripts

from __future__ import print_function
from compiler.ast import flatten
import sys
import numpy as np
from itertools import *
import copy
import time
from collections import defaultdict
from collections import Counter

# For each fold, gets the top N phone seqeuences using phone.wfst and computes the average weighted Levenshtein distance with the bad phoneme.
# For reference, it also computes the weighted Levenshtein distance between the gold and bad phonemes for comparison

# Kulted from http://stackoverflow.com/questions/2460177/edit-distance-in-python
def levenshteinDistance_basic(s1, s2):
	if len(s1) > len(s2):
		s1, s2 = s2, s1

	distances = range(len(s1) + 1)
	for i2, c2 in enumerate(s2):
		distances_ = [i2+1]
		for i1, c1 in enumerate(s1):
			if c1 == c2:
				distances_.append(distances[i1])
			else:
				distances_.append(1 + min((distances[i1], distances[i1 + 1], distances_[-1])))
		distances = distances_
	return distances[-1]

def levenshteinDistance_modified(s1,s2,cmuPhoneList,simMatrix):

	d = [ [ 0 for i in range(0,len(s2)+1) ] for j in range(0,len(s1)+1) ]

	for i in range(0,len(s1)+1):
		d[i][0] = i
	for j in range(0,len(s2)+1):
		d[0][j] = j
#	print d
	for j in range(0,len(s2)):
		for i in range(0,len(s1)):
			if s1[i]==s2[j]:
				subsCost = 0
			else:
				p1i = cmuPhoneList.index(s1[i].rstrip('0123.'))			# Removing numbers also removes stresses, i.e IY0 and IY1 are treated as same
				p2i = cmuPhoneList.index(s2[j].rstrip('0123.'))
				subsCost = 1 - float(simMatrix[p1i][p2i])
#				subsCost = 1
			d[i+1][j+1] = min(d[i][j+1]+1,d[i+1][j]+1,d[i][j]+subsCost)

#	print d
	return d[len(s1)][len(s2)]


fid = open('cmuPhoneList','r')
cmuPhoneList = fid.readlines()
fid.close()
cmuPhoneList = [x.strip('\n') for x in cmuPhoneList]
print('Done')

fid = open('../../data/madgab.pairs','r')			# Dictionary of goodPhones:goodWords
#fid = open('small_madgab.pairs','r')
data = fid.readlines()
fid.close()
data = [x.strip('\n').split(' ') for x in data]
allGoodWords = data[0::5]
allGoodPhones = data[1::5]
allGoodPhones = [' '.join(x) for x in allGoodPhones]
bigDict = dict(zip(allGoodPhones,allGoodWords))

fid = open('../train_test_split/datasets/set_1/test.txt','r')
badPhones = fid.readlines()
fid.close()
badPhones = [x.strip('\n').split() for x in badPhones]
goodPhones = badPhones[0::3]
badPhones = badPhones[1::3]

fid = open('simMatrix.csv','r')
simMatrix = fid.readlines()
fid.close()
simMatrix = [x.strip('\n').split(',') for x in simMatrix]

#found,not_found=0,0
for bpi,bpv in enumerate(badPhones):
	os.system('echo '+' '.join(bpv)+' | carmel -sriQWIEk 100 ../train_test_split/datasets/set_1/phone.wfst > out 2>/dev/null')
	goldWordSeq = bigDict[' '.join(goodPhones[bpi])+' ']

	fid = open('out','r')
	allHyp = fid.readlines()
	fid.close()
	allHyp = [x.strip('\n') for x in allHyp]

	aveLevenDist = 0.0	
	for hypI,hypV in enumerate(allHyp):
		aveLevenDist += levenshteinDistance_modified(bpv,hypV.split(' '),cmuPhoneList,simMatrix)
	aveLevenDist = aveLevenDist/100

	goldLevenDist = levenshteinDistance_modified(bpv,goodPhones[bpi],cmuPhoneList,simMatrix)
	print('Ave hyp dist: '+str(aveLevenDist)+', Gold dist: '+str(goldLevenDist)+'\n')

