#!bin/python


import seq2seq
from seq2seq.models import SimpleSeq2Seq
import numpy as np
from keras.layers.core import Masking
from keras.layers.embeddings import Embedding
from keras.layers import SimpleRNN,LSTM
from keras.models import Sequential
import sys

def levenshteinDistance_modified(s1,s2,cmuPhoneList,simMatrix):
	d = [ [ 0 for i in range(0,len(s2)+1) ] for j in range(0,len(s1)+1) ]
	for i in range(0,len(s1)+1):
		d[i][0] = i
	for j in range(0,len(s2)+1):
		d[0][j] = j
	for j in range(0,len(s2)):
		for i in range(0,len(s1)):
			if s1[i]==s2[j]:
				subsCost = 0
			else:
				p1i = cmuPhoneList.index(s1[i].rstrip('0123.'))			# Removing numbers also removes stresses, i.e IY0 and IY1 are treated as same
				p2i = cmuPhoneList.index(s2[j].rstrip('0123.'))
				subsCost = 1 - float(simMatrix[p1i][p2i])
			d[i+1][j+1] = min(d[i][j+1]+1,d[i+1][j]+1,d[i][j]+subsCost)
	return d[len(s1)][len(s2)]

# Form the dictionary
fid = open('cmuPhoneList','r')
cmuPhoneList = fid.readlines()
fid.close()
cmuPhoneList = [x.strip('\n') for x in cmuPhoneList]
print('Done')
fid = open('simMatrix.csv','r')
simMatrix = fid.readlines()
fid.close()
simMatrix = [x.strip('\n').split(',') for x in simMatrix]


#===================================================== MAIN BEGINS HERE ===================================================

finalLevenMeasure = 0.0
numTest = 0
# Write the outputs for each set
sets = range(1,11)
for setI,setV in enumerate(sets):
	print('Working on fold '+str(setV))
	fid = open('../train_test_split/datasets_no_stress/set_'+str(setV)+'/train.txt','r')
	data = fid.readlines()
	fid.close()
	data = [x.strip(' \n').split(' ') for x in data]
	goodPhones = data[0::3]
	badPhones = data[1::3]	

#	# Read train data
#	fid = open('../../data/noStress_madgab.pairs','r')
#	#fid = open('small_madgab.pairs','r')
#	data = fid.readlines()
#	fid.close()
#	data = [x.strip('\n').split(' ')[:-1] for x in data]
#	badPhones = data[3::5]
#	goodPhones = data[1::5]	

	uniqTrainPhones = list(set(sum(goodPhones,[])+sum(badPhones,[])))
	maxLenGoodPhone = max([len(x) for x in goodPhones])
	maxLenBadPhone = max([len(x) for x in badPhones])

	encoder_input = []
	for gpI,gpV in enumerate(goodPhones):
		encode_array = np.zeros((maxLenGoodPhone,len(uniqTrainPhones)))
		for phoneI,phoneV in enumerate(gpV):
			encode_array[phoneI,uniqTrainPhones.index(phoneV)] = 1
		for phoneI in range(len(gpV),maxLenGoodPhone):
			encode_array[phoneI,:] = -1
		encoder_input.append(encode_array)

	decoder_input = []
	for bpI,bpV in enumerate(badPhones):
		decode_array = np.zeros((maxLenBadPhone,len(uniqTrainPhones)))
		for phoneI,phoneV in enumerate(bpV):
			decode_array[phoneI,uniqTrainPhones.index(phoneV)] = 1
		for phoneI in range(len(bpV),maxLenBadPhone):
			decode_array[phoneI,:] = -1
		decoder_input.append(decode_array)

	encoder_input = np.asarray(encoder_input)
	decoder_input = np.asarray(decoder_input)

	# Model definition, compilation and traininig
	model = Sequential()
	#model.add(Masking(mask_value=-1,input_shape=(encoder_input.shape[1],encoder_input.shape[2])))
	model.add(SimpleSeq2Seq(input_length=maxLenBadPhone, input_dim=len(uniqTrainPhones), hidden_dim=5, output_length=maxLenGoodPhone, output_dim=len(uniqTrainPhones),depth=3))
	model.compile(loss='mse', optimizer='rmsprop')
	model.fit(np.asarray(decoder_input),np.asarray(encoder_input),batch_size=64,nb_epoch=50,validation_split=0.1)	# Other options - Validation split

	# Read in the test data
	fid = open('../train_test_split/datasets_no_stress/set_'+str(setV)+'/test.txt','r')
	data = fid.readlines()
	fid.close()
	data = [x.strip(' \n').split(' ') for x in data]
	testGoodPhones = data[0::3]
	testBadPhones = data[1::3]	

#	test_encoder_input = []
#	for gpI,gpV in enumerate(goodPhones):
#		encode_array = np.zeros((maxLenGoodPhone,len(uniqTrainPhones)))
#		for phoneI,phoneV in enumerate(gpV):
#			encode_array[phoneI,uniqTrainPhones.index(phoneV)] = 1
#		for phoneI in range(len(gpV),maxLenGoodPhone):
#			encode_array[phoneI,:] = -1
#		test_encoder_input.append(encode_array)

	test_decoder_input = []
	for bpI,bpV in enumerate(testBadPhones):
		decode_array = np.zeros((maxLenBadPhone,len(uniqTrainPhones)))
		if len(bpV)>maxLenBadPhone:
			for phoneI in range(maxLenBadPhone):
				decode_array[phoneI,uniqTrainPhones.index(phoneV)] = 1 
		else:
			for phoneI,phoneV in enumerate(bpV):
				decode_array[phoneI,uniqTrainPhones.index(phoneV)] = 1
			for phoneI in range(len(bpV),maxLenBadPhone):
				decode_array[phoneI,:] = -1
		test_decoder_input.append(decode_array)

	# Encode them for use in the NN
	x_pred = model.predict(np.asarray(test_decoder_input))
	x_pred = np.ndarray.tolist(x_pred)
	predictSeq = []
	for seqI,seqV in enumerate(x_pred):
		tempSeq = []
		for phoneI,phoneV in enumerate(seqV):
			if (max(x_pred[seqI][phoneI])>0):
				tempSeq.append(uniqTrainPhones[x_pred[seqI][phoneI].index(max(x_pred[seqI][phoneI]))])
			else:
				break;		# Actually, just break. We don't generally have any more output
#				tempSeq.append('-1')
		predictSeq.append(tempSeq)

#	predictSeq = [' '.join(x) for x in predictSeq]
	
	for i,x in enumerate(testGoodPhones):
		finalLevenMeasure += levenshteinDistance_modified(x,predictSeq[i],cmuPhoneList,simMatrix)
		numTest += 1



# 		SOME WORKING CODE
#encoder_input = []
#for gpI,gpV in enumerate(goodPhones):
#        encode_array = np.zeros((maxLenGoodPhone,len(uniqTrainPhones)))
#        for phoneI,phoneV in enumerate(gpV):
#                encode_array[phoneI,uniqTrainPhones.index(phoneV)] = 10
#        for phoneI in range(len(gpV),maxLenGoodPhone):
#                encode_array[phoneI,:] = -1
#        encoder_input.append(encode_array)

#decoder_input = []
#for bpI,bpV in enumerate(badPhones):
#        decode_array = np.zeros((maxLenBadPhone,len(uniqTrainPhones)))
#        for phoneI,phoneV in enumerate(bpV):
#                decode_array[phoneI,uniqTrainPhones.index(phoneV)] = 10
#        for phoneI in range(len(bpV),maxLenBadPhone):
#                decode_array[phoneI,:] = -1
#        decoder_input.append(decode_array)

#encoder_input = np.asarray(encoder_input)
#decoder_input = np.asarray(decoder_input)

## Model definition, compilation and traininig
#model = Sequential()
##model.add(Masking(mask_value=-1,input_shape=(encoder_input.shape[1],encoder_input.shape[2])))
#model.add(SimpleSeq2Seq(input_length=maxLenBadPhone, input_dim=len(uniqTrainPhones), hidden_dim=20, output_length=maxLenGoodPhone, output_dim=len(uniqTrainPhones),depth=3))
#model.compile(loss='mse', optimizer='rmsprop')
#model.fit(np.asarray(decoder_input),np.asarray(encoder_input),batch_size=64,nb_epoch=25,validation_split=0.1)   # Other options - Validation split







# OLD STUFF

#============================================================================================================
## Testing the seq2seq implementation of keras for predicting good phoneme sequences from mad gabs


## Read train data
#fid = open('../../data/noStress_madgab.pairs','r')
##fid = open('small_madgab.pairs','r')
#data = fid.readlines()
#fid.close()
#data = [x.strip('\n').split(' ')[:-1] for x in data]
#badPhones = data[3::5]
#goodPhones = data[1::5]	

#uniqTrainPhones = list(set(sum(goodPhones,[])+sum(badPhones,[])))
#maxLenGoodPhone = max([len(x) for x in goodPhones])
#maxLenBadPhone = max([len(x) for x in badPhones])


#encoder_input = []
#for gpI,gpV in enumerate(goodPhones):
#	encode_array = np.zeros(maxLenGoodPhone)
##	encode_array = np.zeros((maxLenGoodPhone,len(uniqTrainPhones)))
##	encode_array = np.zeros((len(gpV),len(uniqTrainPhones)))
#	for phoneI,phoneV in enumerate(gpV):
##		encode_array[phoneI,uniqTrainPhones.index(phoneV)] = 1
#		encode_array[phoneI] = uniqTrainPhones.index(phoneV)
#	for phoneI in range(len(gpV),maxLenGoodPhone):
##		encode_array[phoneI,:] = -1
#		encode_array[phoneI] = 0
#	encoder_input.append(encode_array)

#decoder_input = []
#for bpI,bpV in enumerate(badPhones):
#	decode_array = np.zeros(maxLenBadPhone)
##	decode_array = np.zeros((maxLenBadPhone,len(uniqTrainPhones)))
##	decode_array = np.zeros((len(bpV),len(uniqTrainPhones)))
#	for phoneI,phoneV in enumerate(bpV):
#		decode_array[phoneI] = uniqTrainPhones.index(phoneV)
##		decode_array[phoneI,uniqTrainPhones.index(phoneV)] = 1
#	for phoneI in range(len(bpV),maxLenBadPhone):
#		decode_array[phoneI] = 0
##		decode_array[phoneI,:] = -1
#	decoder_input.append(decode_array)

#encoder_input = np.reshape(encoder_input,(len(encoder_input),maxLenGoodPhone,1))
##decoder_input = np.reshape(decoder_input,(len(decoder_input),maxLenBadPhone,1))
#encoder_input = np.asarray(encoder_input)
#decoder_input = np.asarray(decoder_input)

#model = Sequential()
#model.add(Masking(mask_value=0,input_shape=(maxLenGoodPhone,1)))
#model.add(LSTM(maxLenBadPhone))
#model.compile('rmsprop','mse')
#model.fit(encoder_input,decoder_input,batch_size=32,verbose=1,nb_epoch=10)

## Model definition, compilation and traininig
#model = Sequential()
##model.add(Masking(mask_value=-1,input_shape=(encoder_input.shape[1],encoder_input.shape[2])))
#model.add(SimpleSeq2Seq(input_length=maxLenGoodPhone, input_dim=len(uniqTrainPhones), hidden_dim=10, output_length=maxLenBadPhone, output_dim=len(uniqTrainPhones)))
#model.compile(loss='mse', optimizer='rmsprop')
#model.fit(np.asarray(encoder_input),np.asarray(decoder_input),batch_size=64,nb_epoch=25)	# Other options - Validation split

## Evaluate on first 10 seq
#x_pred = model.predict(np.asarray(encoder_input[0:9]))
#x_pred = np.ndarray.tolist(x_pred)
#predictSeq = []
#for seqI,seqV in enumerate(x_pred):
#	tempSeq = []
#	for phoneI,phoneV in enumerate(seqV):
#		if (max(x_pred[seqI][phoneI])>0):
#			tempSeq.append(uniqTrainPhones[x_pred[seqI][phoneI].index(max(x_pred[seqI][phoneI]))])
#		else:
#			tempSeq.append('-1')
#	predictSeq.append(tempSeq)

#sys.exit('Stopping manaully')
## Completely different attempt, not converting to one-hot sequences but masking
#encoder_input = []
#for gpI,gpV in enumerate(goodPhones):
#	encode_array = []
#	for phoneI,phoneV in enumerate(gpV):
#		encode_array.append(uniqTrainPhones.index(phoneV)+1)
#	for phoneI in range(len(gpV),maxLenGoodPhone):
#		encode_array.append(0)
#	encoder_input.append(encode_array)

#decoder_input = []
#for bpI,bpV in enumerate(badPhones):
#	decode_array = []
#	for phoneI,phoneV in enumerate(bpV):
#		decode_array.append(uniqTrainPhones.index(phoneV)+1)
#	for phoneI in range(len(bpV),maxLenBadPhone):
#		decode_array.append(0)
#	decoder_input.append(decode_array)

#encoder_input = np.asarray(encoder_input)
#decoder_input = np.asarray(decoder_input)

#encoder_input = encoder_input.reshape((encoder_input.shape[0],encoder_input.shape[1],1))
#decoder_input = decoder_input.reshape((decoder_input.shape[0],decoder_input.shape[1],1))


#model = Sequential()
#model.add(Embedding(len(uniqTrainPhones)+1,len(uniqTrainPhones)+1,input_length = maxLenGoodPhone,mask_zero=True))
#model.add(SimpleRNN(maxLenBadPhone,input_shape=(maxLenGoodPhone,len(uniqTrainPhones)+1),return_sequences=True))
#model.compile(loss='mse', optimizer='rmsprop')
