#!bin/python


import seq2seq
from seq2seq.models import SimpleSeq2Seq
import numpy as np
from keras.layers.core import Masking
from keras.layers.embeddings import Embedding
from keras.layers import SimpleRNN,LSTM
from keras.models import Sequential
import sys

noStressSwitch = 1		# Meant to be left at 1. Will throw error otherwise

def levenshteinDistance_basic(s1, s2):
	if len(s1) > len(s2):
		s1, s2 = s2, s1

	distances = range(len(s1) + 1)
	for i2, c2 in enumerate(s2):
		distances_ = [i2+1]
		for i1, c1 in enumerate(s1):
			if c1 == c2:
				distances_.append(distances[i1])
			else:
				distances_.append(1 + min((distances[i1], distances[i1 + 1], distances_[-1])))
		distances = distances_
	return distances[-1]

#======================================= MAIN FUNCTION ========================================

# Form the dictionary
fid = open('cmuPhoneList','r')
cmuPhoneList = fid.readlines()
fid.close()
cmuPhoneList = [x.strip('\n') for x in cmuPhoneList]
print('Done')
fid = open('simMatrix.csv','r')
simMatrix = fid.readlines()
fid.close()
simMatrix = [x.strip('\n').split(',') for x in simMatrix]

if (noStressSwitch==0):
	fid = open('../../data/madgab.pairs','r')			# Dictionary of goodPhones:goodWords
else:
	fid = open('../../data/noStress_madgab.pairs','r')
data = fid.readlines()
fid.close()
data = [x.strip('\n').split(' ') for x in data]
allGoodWords = data[0::5]
allGoodPhones = data[1::5]
allGoodPhones = [' '.join(x) for x in allGoodPhones]
bigGoodDict = dict(zip(allGoodPhones,allGoodWords))

allBadWords = data[2::5]
allBadPhones = data[3::5]
allBadPhones = [' '.join(x) for x in allBadPhones]
bigBadDict = dict(zip(allBadPhones,allBadWords))

# Write the outputs for each set

finalCharMeasure = 0.0
numTest = 0
sets = range(1,11)
for setI,setV in enumerate(sets):
	print('Working on fold '+str(setV))
	fid = open('../train_test_split/datasets_no_stress/set_'+str(setV)+'/train.txt','r')
	data = fid.readlines()
	fid.close()
	data = [x.strip(' \n').split(' ') for x in data]
	goodPhones = data[0::3]
	badPhones = data[1::3]	

	goodWords = []
	for gpV in goodPhones:
		goodWords.append(list(' '.join(bigGoodDict[' '.join(gpV)+' '])))	
	badWords = []
	for bpV in badPhones:
		badWords.append(list(' '.join(bigBadDict[' '.join(bpV)+' '])))

	## Read train data
	#fid = open('../../data/noStress_madgab.pairs','r')
	##fid = open('small_madgab.pairs','r')
	#data = fid.readlines()
	#fid.close()
	#data = [x.strip('\n').split(' ')[:-1] for x in data]
	#badPhones = data[3::5]
	#goodPhones = data[1::5]	

	uniqTrainChars = list(set(sum(goodWords,[])+sum(badWords,[])))
	maxLenGoodWord = max([len(x) for x in goodWords])
	maxLenBadWord = max([len(x) for x in badWords])

	encoder_input = []
	for gpI,gpV in enumerate(goodWords):
		encode_array = np.zeros((maxLenGoodWord,len(uniqTrainChars)))
		for phoneI,phoneV in enumerate(gpV):
			encode_array[phoneI,uniqTrainChars.index(phoneV)] = 1
		for phoneI in range(len(gpV),maxLenGoodWord):
			encode_array[phoneI,:] = -1
		encoder_input.append(encode_array)

	decoder_input = []
	for bpI,bpV in enumerate(badWords):
		decode_array = np.zeros((maxLenBadWord,len(uniqTrainChars)))
		for phoneI,phoneV in enumerate(bpV):
			decode_array[phoneI,uniqTrainChars.index(phoneV)] = 1
		for phoneI in range(len(bpV),maxLenBadWord):
			decode_array[phoneI,:] = -1
		decoder_input.append(decode_array)

	encoder_input = np.asarray(encoder_input)
	decoder_input = np.asarray(decoder_input)

#	model = Sequential()
#	model.add(Embedding(len(uniqTrainChars)+1,input_length=maxLenGoodWord,mask_zero=True))
#	model.add(AttentionSeq2Seq(input_length=maxLenGoodWord,input_dim=4,hidden_dim=20,output_length=maxLenBadWord,output_dim=len(uniqTrainChars),depth=1))
#	model.compile(loss='mse', optimizer='rmsprop')

	# Model definition, compilation and traininig
	model = Sequential()
	#model.add(Masking(mask_value=-1,input_shape=(encoder_input.shape[1],encoder_input.shape[2])))
	model.add(SimpleSeq2Seq(input_length=maxLenBadWord, input_dim=len(uniqTrainChars), hidden_dim=100, output_length=maxLenGoodWord, output_dim=len(uniqTrainChars),depth=3))
	model.compile(loss='mse', optimizer='rmsprop')
	model.fit(np.asarray(decoder_input),np.asarray(encoder_input),batch_size=64,nb_epoch=15,validation_split=0.1)	# Other options - Validation split

	# Read in the test data
	fid = open('../train_test_split/datasets_no_stress/set_'+str(setV)+'/test.txt','r')
	data = fid.readlines()
	fid.close()
	data = [x.strip(' \n').split(' ') for x in data]
	testGoodPhones = data[0::3]
	testBadPhones = data[1::3]

	testGoodWords = []
	for gpV in testGoodPhones:
		testGoodWords.append(list(' '.join(bigGoodDict[' '.join(gpV)+' '])))	
	testBadWords = []
	for bpV in testBadPhones:
		testBadWords.append(list(' '.join(bigBadDict[' '.join(bpV)+' '])))

#	test_encoder_input = []
#	for gpI,gpV in enumerate(testGoodWords):
#		encode_array = np.zeros((maxLenGoodWord,len(uniqTrainChars)))
#		for phoneI,phoneV in enumerate(gpV):
#			encode_array[phoneI,uniqTrainChars.index(phoneV)] = 1
#		for phoneI in range(len(gpV),maxLenGoodWord):
#			encode_array[phoneI,:] = -1
#		test_encoder_input.append(encode_array)

	test_decoder_input = []
	for bpI,bpV in enumerate(testBadWords):
		decode_array = np.zeros((maxLenBadWord,len(uniqTrainChars)))
		if len(bpV)>maxLenBadWord:
			for phoneI in range(maxLenBadWord):
				decode_array[phoneI,uniqTrainChars.index(bpV[phoneI])] = 1
		else:
			for phoneI,phoneV in enumerate(bpV):
				decode_array[phoneI,uniqTrainChars.index(phoneV)] = 1
			for phoneI in range(len(bpV),maxLenBadWord):
				decode_array[phoneI,:] = -1
		test_decoder_input.append(decode_array)

	# Encode them for use in the NN
	x_pred = model.predict(np.asarray(test_decoder_input))
	x_pred = np.ndarray.tolist(x_pred)
	predictSeq = []
	for seqI,seqV in enumerate(x_pred):
		tempSeq = []
		for phoneI,phoneV in enumerate(seqV):
			if (max(x_pred[seqI][phoneI])>-0.9):
				tempSeq.append(uniqTrainChars[x_pred[seqI][phoneI].index(max(x_pred[seqI][phoneI]))])
			else:
				break;		# Actually, just break. We don't generally have any more output
#				tempSeq.append('-1')
		predictSeq.append(' '.join(tempSeq))

	for i,x in enumerate(testGoodWords):
		finalCharMeasure += levenshteinDistance_basic(x,predictSeq[i].split())
		numTest+=1


#	if not ' '.join(out):
#		aveLevenDist += len(gpV)
#		aveCharDist += len(goodWordSeq)
#	else:
#		aveLevenDist += levenshteinDistance_modified(gpV,' '.join(out).split(' '),cmuPhoneList,simMatrix)
#		aveCharDist += levenshteinDistance_basic(goodWordSeq,' '.join(allLmOutputs[hypI]))	





#	fid = open(



