#!/bin/python

from __future__ import print_function
from compiler.ast import flatten
import sys
import numpy as np
from itertools import *
import copy
import time
from collections import defaultdict
from collections import Counter

def isclose(a, b, rel_tol=1e-09, abs_tol=0.0):
	return abs(a-b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)

def myCombinatorics(boxes, balls, parent=tuple()):  # Use this to generate all possible combinatorics
	if boxes > 1:
		for i in xrange(balls + 1):
			for x in myCombinatorics(boxes - 1, i, parent + (balls - i,)):
				yield x
	else:
		yield parent + (balls,)

def phoneToWord(ps,cmudict):

	print('Clustering the phones')
	phoneSeq = copy.copy(ps)
	phoneSeq.pop()  # Remove the last element	
	allWordPhrases = []
	N = len(phoneSeq)

	# The most time consuming step. Too many possibilitie
	for numWords in range(2,min(8,int((N+1.0/2)))):						# Each possible hypothesis for number of words
		startTime = time.time()
		for spaceIndex,space_alignment in enumerate(myCombinatorics(N,numWords-1)):	# Each possible space insertion scheme
			
			# Prune the ones with multiple spaces in each position
			flag = 1	
			for x in space_alignment:
				if (x>1):
					flag = 0
					break
			if (flag==0):
				continue

			# Extract phone segments given the space-insertions
			phoneSegs = []
			tempSeg = []
			for segI,segV in enumerate(space_alignment):
				if segV==0:
					tempSeg.append(phoneSeq[segI])
				if segV==1:
					phoneSegs.append(tempSeg)
					tempSeg = []		
					tempSeg.append(phoneSeq[segI])		
			if tempSeg:
				phoneSegs.append(tempSeg)

			# For each phone segment, check if it can constitute a valid word

			wordPhrase = []
			flag=1
			for phoneSegI,phoneSeg in enumerate(phoneSegs):
				# This version is skipped for the sake of speed. the cmudict is now treated as a dictionary
#				try:					
#					# Each phone seg with multiple possible word. Eg: S P AO1 R T S -> SPORTS or SPORT'S or SPORTS'					
#					indices = [i for i, x in enumerate(cmuphones) if x == phoneSeg]
#					if len(indices)>1:						
#						alternateWords = []
#						[alternateWords.append(cmuwords[x]) for x in indices]
#						wordPhrase.append(alternateWords)
#					else:	
#						wordPhrase.append(cmuwords[cmuphones.index(phoneSeg)])					
#				except ValueError:
#					flag=0
#					break

				# Faster version - Correct, handles multiples words per pronunciation		
				try:
					mywords = cmudict[' '.join(phoneSeg)]
					if (len(mywords)>1):
						wordPhrase.append(mywords)
					else:
						wordPhrase.append(''.join(mywords))
				except KeyError:
					flag=0;
					break

			if flag:
				allWordPhrases.append(wordPhrase)
		ftime = time.time()
#		print ('Time for listing '+str(numWords)+ ' combinations is '+str(ftime-startTime))

	startTime = ftime
	# Consider removing candidates if any word contains period (.)
	for i,x in enumerate(allWordPhrases):
		for i2,y in enumerate(x):
			if (isinstance(y,list)):
				allWordPhrases[i][i2] = [z for z in y if '.' not in z]

	# Finally, clean allWordPhrases to handle the multiple words
	newWordPhrases = []
	for ind,elem in enumerate(allWordPhrases):
		lenArr = np.zeros(len(elem))
		for wordI,word in enumerate(elem):
			if (isinstance(word,str)):
				lenArr[wordI] = 1
			elif (isinstance(word,list)):
				lenArr[wordI] = len(word)
			else:
				print('we have a problem')
		
		wordEmbed = np.zeros((int(np.prod(lenArr)),len(lenArr)))

		temp = range(0,int(lenArr[0]))
		for i in range(1,len(lenArr)):
			temp = product(temp,range(0,int(lenArr[i])))
#		if (len(temp)!=len(wordEmbed)):
#			print('ayayayayayooooo')

		for i,x in enumerate(temp):
			wordEmbed[i] = flatten(x)

		for i,x in enumerate(wordEmbed):				
			z = []
			for j,y in enumerate(x):
				if (isinstance(elem[j],str) and wordEmbed[i,j]==0):
					z.append(elem[j])
				elif (isinstance(elem[j],list) and wordEmbed[i,j]==0):
					z.append(elem[j][0])
				elif (isinstance(elem[j],list) and wordEmbed[i,j]>0):
					z.append(elem[j][int(wordEmbed[i,j])])
				else:
					print('we have a problem')
			newWordPhrases.append(z)				

		# SAnity check on wordEmbed to make sure we cover all cases
		x = [tuple(rows) for rows in wordEmbed]
		if (len(set(x))!=len(x)):
			print('problem with word embeddings array')	
	ftime = time.time()
#	print('Time for everything else is '+str(ftime-startTime))
	return newWordPhrases



def rankWordPhrase(newWordPhrases,unigrams,bigrams,OOVstart,OOVend,OOVbigrams):

	print('Ranking the word sequences...')
	# Bigram
	t1 = time.time()
	probCandPhrases = np.zeros(len(newWordPhrases));
	for candPhraseI,candPhrase in enumerate(newWordPhrases):
#		print('scoring on candidate ' + str(candPhraseI))
		try:
			tempval = unigrams[candPhrase[0].lower()][0]
			probCandPhrases[candPhraseI] = tempval
		except KeyError:
			probCandPhrases[candPhraseI] = OOVstart
		for k in range(0,len(candPhrase)-1):		
			try:
				tempval = bigrams[candPhrase[k].lower()+' '+candPhrase[k+1].lower()]
				probCandPhrases[candPhraseI] += tempval
			except KeyError:
				probCandPhrases[candPhraseI] += OOVbigrams				
		try:
			tempval = unigrams[candPhrase[-1].lower()][1]
			probCandPhrases[candPhraseI] += tempval
		except KeyError:
			probCandPhrases[candPhraseI] += OOVend
		t2 = time.time()
#		print(t2-t1)
		t1=t2
		probCandPhrases = probCandPhrases/len(candPhrase)

	a = zip(probCandPhrases,newWordPhrases)
	a.sort(key = lambda row: row[0],reverse=True)

	return a

# Kulted from http://stackoverflow.com/questions/2460177/edit-distance-in-python
def levenshteinDistance_basic(s1, s2):
	if len(s1) > len(s2):
		s1, s2 = s2, s1

	distances = range(len(s1) + 1)
	for i2, c2 in enumerate(s2):
		distances_ = [i2+1]
		for i1, c1 in enumerate(s1):
			if c1 == c2:
				distances_.append(distances[i1])
			else:
				distances_.append(1 + min((distances[i1], distances[i1 + 1], distances_[-1])))
		distances = distances_
	return distances[-1]

def levenshteinDistance_modified(s1,s2,cmuPhoneList,simMatrix):

	d = [ [ 0 for i in range(0,len(s2)+1) ] for j in range(0,len(s1)+1) ]

	for i in range(0,len(s1)+1):
		d[i][0] = i
	for j in range(0,len(s2)+1):
		d[0][j] = j
#	print d
	for j in range(0,len(s2)):
		for i in range(0,len(s1)):
			if s1[i]==s2[j]:
				subsCost = 0
			else:
				p1i = cmuPhoneList.index(s1[i].rstrip('0123.'))			# Removing numbers also removes stresses, i.e IY0 and IY1 are treated as same
				p2i = cmuPhoneList.index(s2[j].rstrip('0123.'))
				subsCost = 1 - simMatrix[p1i][p2i]
#				subsCost = 1
			d[i+1][j+1] = min(d[i][j+1]+1,d[i+1][j]+1,d[i][j]+subsCost)

#	print d
	return d[len(s1)][len(s2)]

# Unigram

#	t1 = time.time()
#	probCandPhrases = np.zeros(len(newWordPhrases));
#	for candPhraseI,candPhrase in enumerate(newWordPhrases):
#		print('scoring on candidate ' + str(candPhraseI))
#		for k in range(0,len(candPhrase)):
##			tempval = [x[0] for x in unigrams if (x[1]==candPhrase[-1].lower())]
#			try:
#				tempval = unigrams[candPhrase[k].lower()][0]
#				probCandPhrases[candPhraseI] += tempval
#			except KeyError:
#				probCandPhrases[candPhraseI] += OOVend
#		t2 = time.time()
#		print(t2-t1)
#		t1=t2


#======================================	BEGIN OF MAIN ============================

print('Reading the dictionary...')
cmudict = open('../../data/cmudict-0.7b_mod')
cmudict = cmudict.readlines()
cmudict = [x.strip('\n').split(' ') for x in cmudict]
cmuwords = [x[0] for x in cmudict]
cmuphones = [' '.join(x[2:]) for x in cmudict]
c = defaultdict(list)					# To tackle multiple words per pronunciation (aka phone segment).
							# cmudict['IY1'] = ('E', 'E.', 'EE'), earlier, ('EE')

for x,y in zip(cmuphones,cmuwords):
	c[x].append(y)
cmudict = dict((k, list(v)) for k, v in c.iteritems())
fid = open('cmuPhoneList','r')
cmuPhoneList = fid.readlines()
fid.close()
cmuPhoneList = [x.strip('\n') for x in cmuPhoneList]
print('Done')

fid = open('../../data/madgab.pairs','r')
#fid = open('small_madgab.pairs','r')
data = fid.readlines()
fid.close()
data = [x.strip('\n').split(' ') for x in data]
goodWords = data[0::5]
goodPhones = data[1::5]			# [3::5] will read the bad phone sequence. [1::5] will read the good phone sequence

print('Reading the N-gram language models. This will take a while...')
fid = open('../../data/unigrams','r')
unigrams = fid.readlines()
fid.close()
unigrams = [x.strip('\n').split('\t') for x in unigrams]
OOVstart = min((float(x[0])) for x in unigrams)
OOVend = min((float(x[2])) for x in unigrams) 
unigramStart = [float(x[0]) for x in unigrams]
unigramEnd = [float(x[-1]) for x in unigrams]
unigrams = [x[1] for x in unigrams]
unigrams = dict(zip(unigrams,zip(unigramStart,unigramEnd)))


fid = open('../../data/bigrams','r')
bigrams = fid.readlines()
fid.close()
bigrams = [x.strip('\n').split('\t') for x in bigrams]
OOVbigrams = min((float(x[0])) for x in bigrams)
bigramVal = [float(x[0]) for x in bigrams]
bigrams = [x[1] for x in bigrams]
bigrams = dict(zip(bigrams,bigramVal))

print('Done')
outFile = open('outfile','w')

allRanks = np.zeros(len(goodPhones))
allHyps = np.zeros(len(goodPhones))
avgLevenDist = np.zeros(len(goodPhones))
fid = open('simMatrix.csv','r')
simMatrix = fid.readlines()
fid.close()
simMatrix = [x.strip('\n').split(',') for x in simMatrix]

bestHyp = []


#print('Beginning to combine the phone sequences')
for phraseIndex,goodPhonePhrase in enumerate(goodPhones):

	wordPhrases = phoneToWord(goodPhonePhrase,cmudict)
	print(str(len(wordPhrases)) + ' word phrase candidates for phone seq ' + str(goodPhonePhrase))

	# Rescore among the candidates absed on a LM

	a = rankWordPhrase(wordPhrases,unigrams,bigrams,OOVstart,OOVend,OOVbigrams);
	b = [x[1] for x in a]
	allHyps[phraseIndex] = len(a)
	if not a:
		print('No candidate word sequences at all!')
		allRanks[phraseIndex] = 0
		avgLevenDist[phraseIndex] = 1.0
		bestHyp.append([' '])
		continue

#	outFile.write('Good Phone seq : \t\t' + ' '.join(goodPhonePhrase)+'\n')
#	outFile.write('Pred word seq : \t\t' + ' '.join(a[0][1])+'\n')
#	outFile.write('Gold word seq : \t\t' + ' '.join(goodWords[phraseIndex])+'\n')

	try:
		rank = b.index(goodWords[phraseIndex])
		print('Gold seq rank : \t\t' + str(rank+1))
#		outFile.write('Gold seq rank : \t\t' + str(rank+1)+'/'+str(allHyps[phraseIndex])+'\n')
		outFile.write(str(rank+1)+'/'+str(allHyps[phraseIndex])+'\n')
		allRanks[phraseIndex] = rank+1
		bestHyp.append(' '.join(b[0]))

	except ValueError:
		print('Gold seq not found in hypothesis')
		bestHyp.append([' '])
		outFile.write('0/'+str(allHyps[phraseIndex])+'\n')
#		outFile.write('Gold seq not found in hypothesis\n')
		allRanks[phraseIndex] = len(a)

	if (len(b)>5):
		c = b[0:5]
	else:
		c = b
	tempDist = []
	for hypI,hypV in enumerate(c):
		tempDist.append(levenshteinDistance_basic(' '.join(hypV),' '.join(goodWords[phraseIndex]),cmuPhoneList,simMatrix))
	avgLevenDist[phraseIndex] = 1.0*sum(tempDist)/(len(tempDist)*len(' '.join(goodWords[phraseIndex])))


print('Levenshtein measure: '+str(sum(avgLevenDist)/len(avgLevenDist)))
# 0.2143 with perfectly transcripts
# 0.6731 with perfectly bad transcritpts (baseline)


matchcount, goldcount, hypcount = 0,0,0
for phraseIndex,goodPhrase in enumerate(goodWords):
# Order : s1 is hyp, s2 is gold

	hypcount += len(bestHyp[phraseIndex])
	goldcount += len(' '.join(goodPhrase))
	cs1 = Counter(bestHyp[phraseIndex])
	cs2 = Counter(' '.join(goodPhrase))

	for x in cs1:
		matchcount += min(cs1[x],cs2[x])

print(' F1 score is '+str(2/(goldcount/float(matchcount) + hypcount/float(matchcount))))

# 0.11446 for our baseline
# 0.89907 with perfectly good phone sequences



outFile.close()
myones,myfives = 0,0
for x in allRanks:
	if x==1:
		myones+=1
	if x<=5 and x>1:
		myfives+=1

print (str(myones)+'/'+str(len(allRanks))+' perfectly correct sentences and '+str(myones+myfives)+'/'+str(len(allRanks))+' sentences with top five hypothesis')



# Compute the average normalized rank (Evaluation metric 2)

#bestPossibleScore = 0.0		# Baseline: 0.99584  Proposed method: 0.51900. Best possible: 0.2777 (Assuming same number of correctly decoded sequences as proposed method)
#modelScore = 0.0
#numDecoded = 0

#for i,x in enumerate(allRanks):
#	if allHyps[i]==0:
#		modelScore += 1.0
#		continue
#	else:
#		numDecoded += 1
#		bestPossibleScore += 1.0/allHyps[i]
#		if x==0:
#			modelScore += 1.0
#		else:
#			modelScore += x/allHyps[i]

#print('Model score: '+str(modelScore))
#print ('Best score: '+str(bestPossibleScore)+'. Model score: '+str(modelScore/numDecoded))


#for i,x in enumerate(allHyps):
#	if (x!=0.0 and x<allRanks[i]):
#		print('Alarm at '+str(i))
