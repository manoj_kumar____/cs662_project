#!bin/python


import seq2seq
from seq2seq.models import SimpleSeq2Seq
import numpy as np
from keras.layers.core import Masking
from keras.layers.embeddings import Embedding
from keras.layers import SimpleRNN,LSTM
from keras.models import Sequential
import sys

#============================================================================================================
# Testing the seq2seq implementation of keras for predicting good phoneme sequences from mad gabs


# Read train data
fid = open('../../data/noStress_madgab.pairs','r')
#fid = open('small_madgab.pairs','r')
data = fid.readlines()
fid.close()
data = [x.strip('\n').split(' ')[:-1] for x in data]
badPhones = data[3::5]
goodPhones = data[1::5]	

uniqTrainPhones = list(set(sum(goodPhones,[])+sum(badPhones,[])))
maxLenGoodPhone = max([len(x) for x in goodPhones])
maxLenBadPhone = max([len(x) for x in badPhones])


encoder_input = []
for gpI,gpV in enumerate(goodPhones):
	encode_array = np.zeros((maxLenGoodPhone,len(uniqTrainPhones)))
	for phoneI,phoneV in enumerate(gpV):
		encode_array[phoneI,uniqTrainPhones.index(phoneV)] = 1
	for phoneI in range(len(gpV),maxLenGoodPhone):
		encode_array[phoneI,:] = -1
	encoder_input.append(encode_array)

decoder_input = []
for bpI,bpV in enumerate(badPhones):
	decode_array = np.zeros((maxLenBadPhone,len(uniqTrainPhones)))
	for phoneI,phoneV in enumerate(bpV):
		decode_array[phoneI,uniqTrainPhones.index(phoneV)] = 1
	for phoneI in range(len(bpV),maxLenBadPhone):
		decode_array[phoneI,:] = -1
	decoder_input.append(decode_array)

encoder_input = np.asarray(encoder_input)
decoder_input = np.asarray(decoder_input)

# Model definition, compilation and traininig
model = Sequential()
#model.add(Masking(mask_value=-1,input_shape=(encoder_input.shape[1],encoder_input.shape[2])))
model.add(SimpleSeq2Seq(input_length=maxLenGoodPhone, input_dim=len(uniqTrainPhones), hidden_dim=10, output_length=maxLenBadPhone, output_dim=len(uniqTrainPhones)))
model.compile(loss='mse', optimizer='rmsprop')
model.fit(np.asarray(encoder_input),np.asarray(decoder_input),batch_size=64,nb_epoch=25)	# Other options - Validation split


# Evaluate on first 10 seq
x_pred = model.predict(np.asarray(encoder_input[0:9]))
x_pred = np.ndarray.tolist(x_pred)
predictSeq = []
for seqI,seqV in enumerate(x_pred):
	tempSeq = []
	for phoneI,phoneV in enumerate(seqV):
		if (max(x_pred[seqI][phoneI])>0):
			tempSeq.append(uniqTrainPhones[x_pred[seqI][phoneI].index(max(x_pred[seqI][phoneI]))])
		else:
			tempSeq.append('-1')
	predictSeq.append(tempSeq)

sys.exit('Stopping manaully')

# Completely different attempt, not converting to one-hot sequences but masking
encoder_input = []
for gpI,gpV in enumerate(goodPhones):
	encode_array = []
	for phoneI,phoneV in enumerate(gpV):
		encode_array.append(uniqTrainPhones.index(phoneV)+1)
	for phoneI in range(len(gpV),maxLenGoodPhone):
		encode_array.append(0)
	encoder_input.append(encode_array)

decoder_input = []
for bpI,bpV in enumerate(badPhones):
	decode_array = []
	for phoneI,phoneV in enumerate(bpV):
		decode_array.append(uniqTrainPhones.index(phoneV)+1)
	for phoneI in range(len(bpV),maxLenBadPhone):
		decode_array.append(0)
	decoder_input.append(decode_array)

encoder_input = np.asarray(encoder_input)
decoder_input = np.asarray(decoder_input)

encoder_input = encoder_input.reshape((encoder_input.shape[0],encoder_input.shape[1],1))
decoder_input = decoder_input.reshape((decoder_input.shape[0],decoder_input.shape[1],1))


model = Sequential()
model.add(Embedding(len(uniqTrainPhones)+1,len(uniqTrainPhones)+1,input_length = maxLenGoodPhone,mask_zero=True))
model.add(SimpleRNN(maxLenBadPhone,input_shape=(maxLenGoodPhone,len(uniqTrainPhones)+1),return_sequences=True))
model.compile(loss='mse', optimizer='rmsprop')


