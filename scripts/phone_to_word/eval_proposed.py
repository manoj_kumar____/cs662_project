#!/bin/python
import os
import sys
import subprocess	

def levenshteinDistance_modified(s1,s2,cmuPhoneList,simMatrix):
	d = [ [ 0 for i in range(0,len(s2)+1) ] for j in range(0,len(s1)+1) ]
	for i in range(0,len(s1)+1):
		d[i][0] = i
	for j in range(0,len(s2)+1):
		d[0][j] = j
	for j in range(0,len(s2)):
		for i in range(0,len(s1)):
			if s1[i]==s2[j]:
				subsCost = 0
			else:
				p1i = cmuPhoneList.index(s1[i].rstrip('0123.'))			# Removing numbers also removes stresses, i.e IY0 and IY1 are treated as same
				p2i = cmuPhoneList.index(s2[j].rstrip('0123.'))
				subsCost = 1 - float(simMatrix[p1i][p2i])
			d[i+1][j+1] = min(d[i][j+1]+1,d[i+1][j]+1,d[i][j]+subsCost)
	return d[len(s1)][len(s2)]

def levenshteinDistance_basic(s1, s2):
	if len(s1) > len(s2):
		s1, s2 = s2, s1

	distances = range(len(s1) + 1)
	for i2, c2 in enumerate(s2):
		distances_ = [i2+1]
		for i1, c1 in enumerate(s1):
			if c1 == c2:
				distances_.append(distances[i1])
			else:
				distances_.append(1 + min((distances[i1], distances[i1 + 1], distances_[-1])))
		distances = distances_
	return distances[-1]

#=========================================================================

baselineSwitch=0
ourMethodSwitch=1
getOutput=0
noStressSwitch=0		# '1' if stress labels are to be ignored. Relevant only if ourMethodSwitch == 1

if getOutput==1:

	if ourMethodSwitch==1:
		# For each test data alignment output in sets 1-10, this script gets the top 10 word hypotheses and outputs in a file lm_output in the respective set
		sets = range(1,11)
		for setV in sets:
			fid = open('../train_test_split/datasets/set_'+str(setV)+'/test.txt','r')
			badPhones = fid.readlines()
			fid.close()
			badPhones = [x.strip('\n').split() for x in badPhones]
		#	goodPhones = badPhones[0::3]
			badPhones = badPhones[1::3]

			os.system('rm -f ../train_test_split/datasets/set_'+str(setV)+'/lm_output')
			for bpI,bpV in enumerate(badPhones):
				os.system('echo '+' '.join(bpV)+' | carmel -sriQWIEk 10 bigram_large.wfst cmudict.fst ../train_test_split/datasets/set_'+str(setV)+'/phone.wfst >> ../train_test_split/datasets/set_'+str(setV)+'/lm_output')


	if baselineSwitch==1:
		sets = range(1,11)
		for setV in sets:
			fid = open('../train_test_split/datasets/set_'+str(setV)+'/test.txt','r')
			badPhones = fid.readlines()
			fid.close()
			badPhones = [x.strip('\n').split() for x in badPhones]
		#	goodPhones = badPhones[0::3]
			badPhones = badPhones[1::3]

			os.system('rm -f ../train_test_split/datasets/set_'+str(setV)+'/final_baseline_output2')	
			for bpI,bpV in enumerate(badPhones):
				os.system('echo '+' '.join(bpV)+' | carmel -sriQWIEk 10 bigram_large.wfst cmudict.fst >> ../train_test_split/datasets/set_'+str(setV)+'/final_baseline_output2')	

#sys.exit('Exiting manually')

#========================= END OF COMPUTATION. NOW OVER TO EVALUATION  (UNTESTED) ===========================


fid = open('cmuPhoneList','r')
cmuPhoneList = fid.readlines()
fid.close()
cmuPhoneList = [x.strip('\n') for x in cmuPhoneList]
print('Done')
fid = open('simMatrix.csv','r')
simMatrix = fid.readlines()
fid.close()
simMatrix = [x.strip('\n').split(',') for x in simMatrix]

if (noStressSwitch==0):
	fid = open('../../data/madgab.pairs','r')			# Dictionary of goodPhones:goodWords
else:
	fid = open('../../data/noStress_madgab.pairs','r')
data = fid.readlines()
fid.close()
data = [x.strip('\n').split(' ') for x in data]
allGoodWords = data[0::5]
allGoodPhones = data[1::5]
allGoodPhones = [' '.join(x) for x in allGoodPhones]
bigDict = dict(zip(allGoodPhones,allGoodWords))


if ourMethodSwitch==1:
	sets = range(1,11)
	finalLevenMeasure = 0.0
	finalCharMeasure = 0.0
	numTest = 0
	for setV in sets:
		print('working on set '+str(setV)+'\n')
		if (noStressSwitch==0):
			fid = open('../train_test_split/datasets/set_'+str(setV)+'/test.txt','r')
		else:
			fid = open('../train_test_split/datasets_no_stress/set_'+str(setV)+'/test.txt','r')			
		badPhones = fid.readlines()
		fid.close()
		badPhones = [x.strip('\n').split() for x in badPhones]
		goodPhones = badPhones[0::3]
	#	badPhones = badPhones[1::3]
		if (noStressSwitch==0):
			fid = open('../train_test_split/datasets/set_'+str(setV)+'/lm_output_64k','r')
		else:
			fid = open('../train_test_split/datasets_no_stress/set_'+str(setV)+'/lm_output_64k','r')	
		allLmOutputs = fid.readlines()
		fid.close()
		allLmOutputs = [x.strip('\n').split(' ')[:-1] for x in allLmOutputs]

		for gpI,gpV in enumerate(goodPhones):
			goodWordSeq = ' '.join(bigDict[' '.join(gpV)+' '])
			aveLevenDist = 0.0
			aveCharDist = 0.0
			for hypI in range(gpI*10,(gpI+1)*10):
				out = []
				for word in allLmOutputs[hypI]:
					out.append(subprocess.check_output('grep "^'+str(word)+'" ../../data/cmudict-0.7b 2>/dev/null | head -n1 | cut -f 2- -d " " | sed "s/^ *//g"',shell=True).strip('\n'))

				if (noStressSwitch==1):
					out = ' '.join(out)
					out = ''.join([i for i in out if not i.isdigit()])		
					out = out.split(' ')		# Some crazy hacks for splitting and joining

				if not ' '.join(out):
					aveLevenDist += len(gpV)
					aveCharDist += len(goodWordSeq)
				else:
					aveLevenDist += levenshteinDistance_modified(gpV,' '.join(out).split(' '),cmuPhoneList,simMatrix)
					aveCharDist += levenshteinDistance_basic(goodWordSeq,' '.join(allLmOutputs[hypI]))
			aveLevenDist = aveLevenDist/10
			aveCharDist = aveCharDist/10

			finalLevenMeasure += aveLevenDist
			finalCharMeasure += aveCharDist
			numTest += 1
	
	print("Weighted Levenshtein measure is "+str(finalLevenMeasure/numTest)+' and character edit distance is '+str(finalCharMeasure/numTest)+'\n')
		

if baselineSwitch==1:
# baseline evaluation:

	sets = range(1,11)
	finalLevenMeasure = 0.0
	finalCharMeasure = 0.0
	numTest = 0
	for setV in sets:
		print('working on set '+str(setV)+'\n')
		fid = open('../train_test_split/datasets/set_'+str(setV)+'/test.txt','r')
		badPhones = fid.readlines()
		fid.close()
		badPhones = [x.strip('\n').split() for x in badPhones]
		goodPhones = badPhones[0::3]
#		badPhones = badPhones[1::3]

		fid = open('../train_test_split/datasets/set_'+str(setV)+'/baseline_ouput_64k','r')
		allLmOutputs = fid.readlines()
		fid.close()
		allLmOutputs = [x.strip('\n').split(' ')[:-1] for x in allLmOutputs]

		for gpI,gpV in enumerate(goodPhones):
			goodWordSeq = ' '.join(bigDict[' '.join(gpV)+' '])
			aveLevenDist = 0.0
			aveCharDist = 0.0
			for hypI in range(gpI*10,(gpI+1)*10):
				out = []
				for word in allLmOutputs[hypI]:
					out.append(subprocess.check_output('grep "^'+str(word)+'" ../../data/cmudict-0.7b 2>/dev/null | head -n1 | cut -f 2- -d " " | sed "s/^ *//g"',shell=True).strip('\n'))
				if not out:
					aveLevenDist += len(gpV)
					aveCharDist += len(goodWordSeq)
				else:
					aveLevenDist += levenshteinDistance_modified(gpV,' '.join(out).split(' '),cmuPhoneList,simMatrix)
					aveCharDist += levenshteinDistance_basic(goodWordSeq,' '.join(allLmOutputs[hypI]))
			aveLevenDist = aveLevenDist/10
			aveCharDist = aveCharDist/10

			finalLevenMeasure += aveLevenDist
			finalCharMeasure += aveCharDist
			numTest += 1
	
	print("Weighted Levenshtein measure is "+str(finalLevenMeasure/numTest)+' and character edit distance is '+str(finalCharMeasure/numTest)+'\n')
