import pdb

with open('no_stress', 'r') as f:
    no_stress = f.readlines()
with open('stress', 'r') as f:
    stress = f.readlines()


for it in range(0, len(stress)):
    print it    

    fns = no_stress[it]
    fs = stress[it]
    
    with open(fns[0:-1], 'r') as f:
        data_ns = f.readlines()
    with open(fs[0:-1], 'r') as f:
        data_s = f.readlines()

    data = []
    for i in range(0, len(data_ns)):
        if (i+1)%3 == 0:
            data.append(data_ns[i])
        else:
            data.append(data_s[i])
    elms = fns.split('/')
    elms[-1] = elms[-1][0:-2]+'.stressed'
    f_new = '/'.join(elms)

    with open(f_new, 'a') as f:
        for line in data:
            f.write(line)

