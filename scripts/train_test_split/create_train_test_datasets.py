import os
import pdb
import numpy as np

indx=[]
with open('k_fold_indices.txt') as f:
    indx_list = f.readlines()
for elm in indx_list:
    indx.append(int(elm))

with open('../phone_to_phone_wfst/data/good_phone_bad_phone.unsupervised') as f:
    data = f.readlines()

indx=np.array(indx)
for it in range(1, 11):
    #indices = [i for i, x in enumerate(indx) if x == it]
    test_indx = indx == it
    train_indx = indx != it

    os.mkdir('datasets/set_'+str(it)+'/')

    for i in range(0, len(test_indx)):
        if test_indx[i] == True:
            with open('datasets/set_'+str(it)+'/test.txt', 'a') as f:
                f.write(data[3*i])
                f.write(data[3*i+1])
                f.write(data[3*i+2])
    for i in range(0, len(train_indx)):
        if train_indx[i] == True:
            with open('datasets/set_'+str(it)+'/train.txt', 'a') as f:
                f.write(data[3*i])
                f.write(data[3*i+1])
                f.write(data[3*i+2])
     




