for folder in datasets_no_stress/*
do
    echo $folder
    file=$folder'/test.txt'
    num_lines=`wc -l $file | awk '{print $1}'`
    for (( i=3; i<=$num_lines; ++i ))
    do
        num=$(( $i -1 ))
        input=`sed -n $num'p' $file`
        echo $input | carmel -sriIEQk 1 $folder'/phone.wfst' >> $folder'/test_best_outcome.txt'
        i=$(( $i + 2 ))
    done
done    
