import pdb
import numpy as np
from copy import deepcopy

with open('good_phone_bad_phone.unsupervised') as f:
    data = f.readlines()

for iteration in range(0, 13, 3):

    #w1 = '"AH" "K" "EY" "SH" "AH"'
    #w2 = '"A" "K" "A" "SH" "I" "A"'

    if len( data[iteration].split() ) <= len( data[iteration+1].split() ):
        w1 = data[iteration]
        w2 = data[iteration+1]
    else:
        w2 = data[iteration]
        w1 = data[iteration+1]


    l1 = w1.split()
    l2 = w2.split()


    n1=len(l1)
    n2=len(l2)
    delta=n2-n1

    whole=[]
    for i in range(1,n2+1):
        #print 'i=',i
        curr=[]
        for j in range(min(i,n1), (i-delta-1), -1):
            #print 'j=',j
            if j<1:
                break
            curr.append(j)
        whole.append(np.sort(curr))
            
    #print whole


    all_combs = [[1]]
    cnt=0
    for i in range(1, len(whole)):
        l = len(all_combs)
        for j in range(0, l):
            prev = all_combs[j][-1]
            same_val_flag = 0
            for k in range(0,len(whole[i])):
                if whole[i][k] < prev:
                    continue
                if whole[i][k] == prev:
                    all_combs[j].append(whole[i][k])    
                    same_val_flag = 1
                if whole[i][k] ==  prev+1:
                    if same_val_flag == 1:
                        tmp = deepcopy(all_combs[j])
                        tmp[-1] = whole[i][k]
                        all_combs.append(tmp)
                        cnt=cnt+1
                    else: 
                        if same_val_flag == 0:
                            all_combs[j].append(whole[i][k])
                        
    def printListElement(elem):
        string=''
        for i in range(0, len(elem)):
            if i != len(elem)-1:
                string = string + str(elem[i]) + ' '
            else:
                string = string + str(elem[i])
        print string    

    print w1
    print w2
    for i in range(0, len(all_combs)):
        printListElement(all_combs[i])


