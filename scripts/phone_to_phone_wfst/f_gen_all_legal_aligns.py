import pdb
import numpy as np
from copy import deepcopy

def get_all_aligns(word1, word2):

    if len( word1.split() ) <= len( word2.split() ):
        w1 = word1
        w2 = word2
    else:
        w2 = word1
        w1 = word2


    l1 = w1.split()
    l2 = w2.split()


    n1=len(l1)
    n2=len(l2)
    delta=n2-n1

    whole=[]
    for i in range(1,n2+1):
        #print 'i=',i
        curr=[]
        for j in range(min(i,n1), (i-delta-1), -1):
            #print 'j=',j
            if j<1:
                break
            curr.append(j)
        whole.append(np.sort(curr))
            
    #print whole


    all_combs = [[1]]
    cnt=0
    for i in range(1, len(whole)):
        l = len(all_combs)
        for j in range(0, l):
            prev = all_combs[j][-1]
            same_val_flag = 0
            for k in range(0,len(whole[i])):
                if whole[i][k] < prev:
                    continue
                if whole[i][k] == prev:
                    all_combs[j].append(whole[i][k])    
                    same_val_flag = 1
                if whole[i][k] ==  prev+1:
                    if same_val_flag == 1:
                        tmp = deepcopy(all_combs[j])
                        tmp[-1] = whole[i][k]
                        all_combs.append(tmp)
                        cnt=cnt+1
                    else: 
                        if same_val_flag == 0:
                            all_combs[j].append(whole[i][k])
    
    return all_combs

                    

