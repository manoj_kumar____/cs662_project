Phone to phone conversion WFST:

Main files to run:
1. EM.py: The basic file for learning the phone changes. 
    input:  Pairs of good phone sequence and mad gab sequences written in the 
            following format:   
            AH0 B IY1 S IY1 S P AO1 R T S
            EY1 B IY1 Z S IY1 Z S AH0 P AO1 R T S
            
            AH0 B AA1 T AH0 L AH1 V K OW1 K
            EY1 B AA1 D HH AH1 L L AH1 K OW1 K

            ...
            ...
    
    output: Alignment in the following format.
            AH0 B IY1 S IY1 S P AO1 R T S
            EY1 B IY1 Z S IY1 Z S AH0 P AO1 R T S
            1 2 3 4 4 5 6 6 6 7 8 9 10 11
            AH0 B AA1 T AH0 L AH1 V K OW1 K
            EY1 B AA1 D HH AH1 L L AH1 K OW1 K
            1 2 3 4 5 5 6 7 8 9 10 11

    default input: data/good_phone_bad_phone.unsupervised

2. create_data_set.py: To create the data input of EM.py from the original data
                       file ../../data/madgab.pairs.

3. train_test_split/gen_cv_results.sh:

    Use this to generate phoneme.alignment result for 10 folds of data directly.
    It uses EM.py as function to generate the alignment results for each fold
    and saves there. 
    It also generates "phone.wfst" in carmel format from the phoneme.alignment
    file in each fold. This phone.wfst is used in the pipeline of our system
    for implementing phone changes.

    default input:  ../../train_test_split/datasets_no_stress/
                    It works on each fold named set_i (i=1:10) in the above 
                    directory.



Methodology used to phone-to-phone conversion:
Noisy channel model:

                          |                                   |
good phoneme(s) ------->  | P(bad phoneme(s)|good phoneme(s)) | --------> bad phoneme(s)
                          |                                   |


Example data pair:
Good: G EY1 B R IY0 AH0 L       (for GABRIEL)
Bad:  G EY1 B R IY1 Y EH1 L     (for GAY BRIE YELL)

Alignment options:
1. One to one
2. One to consecutive many
3. Consecutive many to one
4. No overlap

Analogy with HW3 transliteration problem:
english = good
japanese = bad

Only diff:
There |jap| >= |eng|, here this restriction is not there.

output of EM.py:
results/goodphone-badphone.alignment
How to read the output?
{
    str1
    str2
    number_str
}
|number_str| = |argmax(|str1|, |str2|)|
Align the longer string's phones with shorter string's phones. In this way, you can read
the many-to-one mappings.
