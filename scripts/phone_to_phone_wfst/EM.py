import numpy as np
import pdb
from f_gen_all_legal_aligns import get_all_aligns

def get_string_from_array(arr):
    align_str=''
    for i in range(0, len(arr)):
        if i != len(arr)-1:
            align_str = align_str + str(arr[i]) + ' '
        else:
            align_str = align_str + str(arr[i])
    return align_str

#read input file
with open('data/good_phone_bad_phone.unsupervised') as f:
    data = f.readlines()
#clean output files
with open('results/first_five.txt', 'w') as f:
    f.write('')
with open('results/goodphone-badphone.alignment', 'w') as f:
    f.write('')


list_eng_sound = []
#get all distinct english sound sequences
for i in range(0, len(data)-2, 3):
    eng = data[i].split()
    for j in range(0, len(eng)):
        try:
            ind = list_eng_sound.index(eng[j])
        except ValueError:
            list_eng_sound.append(eng[j])

count={}
prob = {}
iteration = 0
max_iterations = 5

while(iteration < max_iterations):
    print 'Iteration = ', iteration
    with open('results/first_five.txt', 'a') as f:
        f.write('\n\nIteration = ' + str(iteration) + '\n')
        f.write('------------------------------------\n')

    for word_pair in range(0, len(data)-2, 3):

        eng = data[word_pair]
        jap = data[word_pair+1]
        eng_ls = eng.split()
        jap_ls = jap.split()
        
        r1=len(jap_ls)
        r2=len(eng_ls)
        if r1 > r2:
            r = r2
        else:
            r = r1

        best_score_for_this_pair = 0
        best_indx_for_this_pair = 0

        #2a. enumerate all legal alignments a1...an
        list_all_aligns = get_all_aligns(eng, jap)
        n = len(list_all_aligns)

        #2b. compute alignment probabilities P(a1)...P(an) as follows:
        P = np.empty(n)
        if iteration == 0:
            P.fill(1/float(n))
            align_str = get_string_from_array(list_all_aligns[0])
            if word_pair < 16:
                with open('results/first_five.txt', 'a') as f:
                    f.write(eng[0:-2] + '\n' + jap[0:-2] + '\n' + align_str + '\n')
        else:
            for i in range(0, n):
                P[i]=1.0
                for k in range(0,r):
                    indx = [itr for itr, x in enumerate(list_all_aligns[i]) if x == k+1]
                    if r==r2: 
                        e_k = eng_ls[k]
                        jap_str = ''
                        for itr2 in range(0, len(indx)):
                            if itr2==0:
                                jap_str = jap_str + str(jap_ls[indx[itr2]])
                            else:
                                jap_str = jap_str + '_' + str(jap_ls[indx[itr2]])
                            
                        key = jap_str + '|' + str(e_k)
                        
                    else:
                        j_k = jap_ls[k]
                        eng_str = ''
                        for itr2 in range(0, len(indx)):
                            if itr2==0:
                                eng_str = eng_str + str(eng_ls[indx[itr2]])
                            else:
                                eng_str = eng_str + '_' + str(eng_ls[indx[itr2]])
                        
                        #update list of distinct english sounds
                        if eng_str not in list_eng_sound:
                            list_eng_sound.append(eng_str)
        
                        key = str(j_k) + '|' + eng_str

                    P[i] = P[i] * prob[key]

            #normalize to yield P(ai) estimates
            total=0.0
            for i in range(0, n):
                total = total + P[i]
            for i in range(0, n):
                P[i] = P[i]/total
                if P[i] > best_score_for_this_pair:
                    best_score_for_this_pair = P[i]
                    best_indx_for_this_pair = i
            #good place to print best alignment for this word pair
            align_str = get_string_from_array(list_all_aligns[best_indx_for_this_pair])
            if iteration == max_iterations-1:
                with open('results/goodphone-badphone.alignment', 'a') as f:
                    f.write(eng[0:-2] + '\n' + jap[0:-2] + '\n' + align_str + '\n')
            if word_pair < 16:
                with open('results/first_five.txt', 'a') as f:
                    #f.write(eng + '\n' + jap + '\n' + align_str + '\n')                            
                    f.write(eng[0:-2] + '\n' + jap[0:-2] + '\n' + align_str + '\n')                            
    
        #2c. collect fractional counts over all legal alignments as follows:
        for i in range(0, n):
            for k in range(0, r):
                indx = [itr for itr, x in enumerate(list_all_aligns[i]) if x == k+1]
                if r==r2: 
                    e_k = eng_ls[k]
                    jap_str = ''
                    for itr2 in range(0, len(indx)):
                        if itr2==0:
                            jap_str = jap_str + str(jap_ls[indx[itr2]])
                        else:
                            jap_str = jap_str + '_' + str(jap_ls[indx[itr2]])
                        
                    key = jap_str + '|' + str(e_k)
                    try:
                        count[key] = count[key] + P[i]    
                    except KeyError:
                        count[key] = P[i]

                else:
                    j_k = jap_ls[k]
                    eng_str = ''
                    for itr2 in range(0, len(indx)):
                        if itr2==0:
                            eng_str = eng_str + str(eng_ls[indx[itr2]])
                        else:
                            eng_str = eng_str + '_' + str(eng_ls[indx[itr2]])
                    
                    #update list of distinct english sounds
                    if eng_str not in list_eng_sound:
                        list_eng_sound.append(eng_str)
    
                    key = str(j_k) + '|' + eng_str
                    try:
                        count[key] = count[key] + P[i]    
                    except KeyError:
                        count[key] = P[i]

             
    #normalize sound-translation counts to yield P(J | e) estimates as follows:
    for i in range(0, len(list_eng_sound)):
        eng_sound_distinct = list_eng_sound[i]
        total = 0
        for key in count.keys():
            eng_sound_frm_key = key.split('|')[1]
            if eng_sound_frm_key == eng_sound_distinct:
                 if count[key] > 0:
                    total = total + count[key]
        for key in count.keys():
            eng_sound_frm_key = key.split('|')[1]
            if eng_sound_frm_key == eng_sound_distinct:
                if count[key] > 0:
                    prob[key] = count[key] / total


    #4. clear counts as follows:
    for key in count.keys():
        if count[key]>0:
            count[key] = 0

    iteration = iteration + 1


