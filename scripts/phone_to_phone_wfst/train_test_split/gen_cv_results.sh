for folder in ../../train_test_split/datasets_no_stress/*
do
    if [[ $folder != *"set_"* ]]; then
        echo $folder
        continue
    fi
    for file in $folder/train*
    do 
        echo $file
        dir=`dirname $file`'/'
        python f_EM.py $file $dir'phone.alignment'
        python make_wfst.py $dir'phone.alignment' $dir'phone.wfst'
    done
done
