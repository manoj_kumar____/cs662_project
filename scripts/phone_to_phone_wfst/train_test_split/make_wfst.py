#analogy with hw#3
#english=good phrase, japanese=bad phrase

import pdb
import numpy as np
import sys

print list(sys.argv)
inputs = list(sys.argv)

with open(inputs[1]) as f:
#with open('smaller.data') as f:
    data = f.readlines()

data_len = len(data)

################################# functions #####################################
def f_unq_list(seq):
   # Not order preserving
    keys = {}
    for e in seq:
        keys[e] = 1
    return keys.keys()

def get_tokens(word):
    tokens = word.split()
    len_word = len(tokens)
    return tokens, len_word
def prepare_key_from_mult_tokens(tokens):
    out=tokens[0]
    for t in range(0,len(tokens)):
        if t>0:
            out=out+'_'+tokens[t]
    return out

wfst = {}
print 'Calculating probabilities...'
for it in range(0, data_len-2, 3):
    print 'Iter = ', it
    eng_word = data[it]
    jap_word = data[it+1]
    mapping = data[it+2]

    eng_tokens, eng_num_tokens = get_tokens(eng_word)
    jap_tokens, jap_num_tokens = get_tokens(jap_word)
    map_tokens, map_num_tokens = get_tokens(mapping)


    if jap_num_tokens >= eng_num_tokens:
        for j in range(0, eng_num_tokens):
            key = eng_tokens[j] + '->'
            indices = [i for i, x in enumerate(map_tokens) if x == str(j+1)]
            val = ''
            for ind in indices:
                val += jap_tokens[ind] + '_'
            val = val[0:-1]
        
            key += val
            try:
                wfst[key] += 1.0
            except KeyError:
                wfst[key] = 1.0

    else:
        for j in range(0, jap_num_tokens):
            indices = [i for i, x in enumerate(map_tokens) if x == str(j+1)]     
             
            val = ''
            for ind in indices:
                val += eng_tokens[ind] + '_'
            val = val[0:-1]
        
            key = val + '->' + jap_tokens[j]
            try:
                wfst[key] += 1.0
            except KeyError:
                wfst[key] = 1.0


count = {}
for key in wfst.keys():
    left = key.split('->')[0]
    leftmost = left.split('_')[0]
    try:
        count[leftmost] += wfst[key]
    except KeyError:
        count[leftmost] = wfst[key]

for key in wfst.keys():
    left = key.split('->')[0]
    leftmost = left.split('_')[0]
    wfst[key] /= count[leftmost]
    


with open(inputs[2], 'w') as f:
    f.write('0\n')


state_count = 1 #start and stop states are 0
with open(inputs[2], "a") as myfile:
    for key in wfst.keys():
#        print 'For the map: '+key
        left = key.split('->')[0]
        right = key.split('->')[1]
        left_tokens = left.split('_')
        right_tokens = right.split('_')


        value = wfst[key]


        if len(left_tokens) == 1 and len(right_tokens) == 1:
            string = '(0 (0 ' + left + ' ' + right + ' ' + str(value) + '))'
            myfile.write(string+'\n')
        else:
            #pdb.set_trace()
            if len(left_tokens) == 1:
                for j in range(0, len(right_tokens)):
                    if j == 0:
                        string = '(0 (' + str(state_count) + ' ' + left + ' ' + right_tokens[j] + ' ' + str(value) + '))'
                        myfile.write(string+'\n')
                    else:
                        if j != len(right_tokens) - 1:
                            string = '(' + str(state_count) + ' (' + str(state_count+1) + ' ' + '*e*' + ' ' + right_tokens[j] + ' ' + str(1.0) + '))'
                            myfile.write(string+'\n')
                        else:
                            string = '(' + str(state_count) + ' (' + str(0) + ' ' + '*e*' + ' ' + right_tokens[j] + ' ' + str(1.0) + '))'
                            myfile.write(string+'\n')

                        state_count = state_count + 1

            else: 
                for j in range(0, len(left_tokens)):
                    if j==0:
                        string = '(0 (' + str(state_count) + ' ' + left_tokens[0] + ' ' + '*e*' + ' ' + str(value) + '))'
                        myfile.write(string+'\n')
                    else:
                        if j != len(left_tokens) - 1:
                            string = '(' + str(state_count) + ' (' + str(state_count+1) + ' ' + left_tokens[j] + ' ' + '*e*' + ' ' + str(1.0) + '))'
                            myfile.write(string+'\n')
                        else:
                            string = '(' + str(state_count) + ' (' + str(0) + ' ' + left_tokens[j] + ' ' + right + ' ' + str(1.0) + '))'
                            myfile.write(string+'\n')
                       
                        state_count = state_count + 1

print 'Done'
 




