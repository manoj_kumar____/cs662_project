import numpy as np
import pdb


with open('../../data/madgab.pairs') as f:
    data = f.readlines()

for i in range(0, len(data)-4, 5):
    with open('good_phone_bad_phone.unsupervised', 'a') as f:
        f.write(data[i+1])   
        f.write(data[i+3])
        f.write('\n')
