#!/bin/python

from itertools import izip
import sys

skipPart1 = 1		# Switch this on to skip creation of OOV word pronunciations

fid2 = open('../data/cmudict-0.7b','r')
fullDict = fid2.readlines()
fid2.close()
d = [x.split(' ')[0] for x in fullDict]
fullDict = [x.strip('\n') for x in fullDict]

# Read in all mad-gabs and normal words 
fid1 = open('../data/bad.madgabs','r')
b = fid1.readlines()
fid1.close()
b = [x.strip('\t\n').split(' ') for x in b]
b = list(set(sum(b,[])))
#print(str(len(b)) + ' unique words')
del b[b.index('')]

fid1 = open('../data/good.madgabs','r')
g = fid1.readlines()
fid1.close()
g = [x.strip('\t\n').split(' ') for x in g]
g = list(set(sum(g,[])))
#print(str(len(b)) + ' unique words')

OOVb = []
for wordInd,wordVal in enumerate(b):
	try:
		myIndex = d.index(wordVal.upper())
	except ValueError:
		OOVb.append(wordVal)	
print(str(len(OOVb)) + ' OOV words in bad madgabs')

OOVg = []
for wordInd,wordVal in enumerate(g):
	try:
		myIndex = d.index(wordVal.upper())
	except ValueError:
		OOVg.append(wordVal)	
print(str(len(OOVg)) + ' OOV words in good madgabs')

if (skipPart1 == 0):
	fid3 = open('../data/logios.txt','w')

	for word in OOVb:
		fid3.write(word.upper() + '\n')

	for word in OOVg:
		fid3.write(word.upper() + '\n')

	fid3.close()	


# Now, create one file with pronunciations of madgab pairs

fid4 = open('../data/madgab.pairs','w')
fid5 = open('../data/logios_output.dict','r')
oovPron = fid5.readlines()
oovPron = [x.strip('\n').split(' ') for x in oovPron]
oovWord = [x[0] for x in oovPron]

with open("../data/good.madgabs") as fidg, open("../data/bad.madgabs") as fidb: 
    for x, y in izip(fidg, fidb):
        x = x.strip()
        y = y.strip()
	
	fid4.write(x.upper() + '\n')
	x = x.split(' ')
	for gword in x:
		try:
			gPron = fullDict[d.index(gword.upper())].split(' ')[1:]
			gPron = filter(lambda a: a != '',gPron)
			for z in gPron:
				fid4.write(z + ' ')
		except ValueError:
			gPron = oovPron[oovWord.index(gword.upper())][1:]
			try:
				gPron = filter(lambda a: a != '',gPron)
			except ValueError:
				print('needed')
			for z in gPron:
				fid4.write(z + ' ')
	fid4.write('\n')


	fid4.write(y.upper() + '\n')
	y = y.split(' ')
	for bword in y:
		try:
			bPron = fullDict[d.index(bword.upper())].split(' ')[1:]
			bPron = filter(lambda a: a != '',bPron)
			for z in bPron:
				fid4.write(z + ' ')
		except ValueError:
			bPron = oovPron[oovWord.index(bword.upper())][1:]
			try:
				bPron = filter(lambda a: a != '',bPron)
			except ValueError:
				print('needed')
			for z in bPron:
				fid4.write(z + ' ')
	fid4.write('\n\n')

#        print("{0}\t{1}".format(x, y))

fid4.close()
fid5.close()
# Create list of OOV words
