##	Automatic Solver for Mad Gab


Mad gab is a popular multi-player game where the goal is to decipher a seemingly nonsense sounding collection of words (mad gab) into a popular phrase (true phrase). Some examples:

Mad Gab            	->    True Phrase
Of lions quarrel        ->    A flying squirrel
Eye mull of mush sheen  ->    I’m a love machine


Using basic NLP techniques, we build an automatic mad gab solver. In this report, we use a list of publicly available mad gabs and learn the phoneme changes between the mad gab and true phrase using Expectation Maximization technique (EM). For evaluation purposes, we have split our dataset into 10 parts and iteratively use one part for testing. The work constitutes a course project and contains a baseline method, a proposed approach (EM) and a few additional improvements. 

The dataset we use comes from the following sources:  
https://quizlet.com/  
https://flashcards.engrade.com/madgab  
http://www.cram.com/flashcards/mad-gab-6151468  
http://docslide.us/documents/mad-gab-flashcards-quizlet.html  
http://www.thinkablepuzzles.com/madgabs/  
For generating the phoneme sequences, we use CMU pronunciation dictionary and LOGIOS tool. Finally, we make use of Carmel to implement our WFSTs

The baseline method assumes that the phoneme sequence between the mad gab and true phrase remain the same. The proposed method can be broadly divided into two categories:

## Phone to phone conversion WFST:

Methodology used to phone-to-phone conversion:
Noisy channel model:
                          |                                   |
good phoneme(s) ------->  | P(bad phoneme(s)|good phoneme(s)) | --------> bad phoneme(s)
                          |                                   |
Example data pair:
Good: G EY1 B R IY0 AH0 L       (for GABRIEL)
Bad:  G EY1 B R IY1 Y EH1 L     (for GAY BRIE YELL)

Alignment options:
1. One to one
2. One to consecutive many
3. Consecutive many to one
4. No overlap

Analogy with HW3 transliteration problem:
english = good
japanese = bad

Only diff:
There |jap| >= |eng|, here this restriction is not there.

output of EM.py:
results/goodphone-badphone.alignment
How to read the output?
{
    str1
    str2
    number_str
}
|number_str| = |argmax(|str1|, |str2|)|
Align the longer string's phones with shorter string's phones. In this way, you can read
the many-to-one mappings.

Main files to run:  
1. EM.py: The basic file for learning the phone changes.
    input:  Pairs of good phone sequence and mad gab sequences written in the
            following format:   
            AH0 B IY1 S IY1 S P AO1 R T S
            EY1 B IY1 Z S IY1 Z S AH0 P AO1 R T S
            
            AH0 B AA1 T AH0 L AH1 V K OW1 K
            EY1 B AA1 D HH AH1 L L AH1 K OW1 K


            ...
            ...
    
    output: Alignment in the following format.
            AH0 B IY1 S IY1 S P AO1 R T S
            EY1 B IY1 Z S IY1 Z S AH0 P AO1 R T S
            1 2 3 4 4 5 6 6 6 7 8 9 10 11
            AH0 B AA1 T AH0 L AH1 V K OW1 K
            EY1 B AA1 D HH AH1 L L AH1 K OW1 K
            1 2 3 4 5 5 6 7 8 9 10 11


    default input: data/good_phone_bad_phone.unsupervised

2. create_data_set.py: To create the data input of EM.py from the original data
                       file ../../data/madgab.pairs.

3. train_test_split/gen_cv_results.sh:

    Use this to generate phoneme.alignment result for 10 folds of data directly.
    It uses EM.py as function to generate the alignment results for each fold
    and saves there.
    It also generates "phone.wfst" in carmel format from the phoneme.alignment
    file in each fold. This phone.wfst is used in the pipeline of our system
    for implementing phone changes.

    default input:  ../../train_test_split/datasets_no_stress/
                    It works on each fold named set_i (i=1:10) in the above
                    directory.

## Phone to word conversion and hypotheses ranking:

All the generated phone sequences using EM are converted into word sequences using the CMU pronunciation dictionary. For each sequence, all possible combinations of subsequences are considered which can possibly form words. Obviously, a word sequence is complete only if all phone subsequences combine into legitimate words. For this task, the CMU dictionary is used to create a WFST with equal weight for all words. The WFST is created as follows:

python scripts/phone_to_word/createCMUfst.py

Use the variable noStressSwitch to toggle between using/discarding the stress makers.

The word sequences coming from cmudict.wfst are ranked using a bigram LM. We use the Gigaword corpus with 64k vocabulary for this purpose. The WFST (bigram_large.wfst) mirrors the input with a probability equal to P(sentence) = P(word1|start)*P(word2|word1)....

The main script for evaluation is eval_proposed.py. It includes the code for generating the top 10 word hypotheses starting with mad gab phone sequence (toggle with getOutput variable), and computing the two evaluation metrics:  
levenshteinDistance_modified: Mean weighted edit distance between the phoneme sequences of the mad gab and top 10 hypotheses  
levenshteinDistance_basic: Mean edit distance between the character sequences of the mad gab and top 10 hypotheses  

Run the evaluation as:

Python eval_proposed.py

Important variables to control for:
getOutput        -> To generate the top 10 word hypotheses  
baselineSwitch    -> To evaluate the baseline method instead  
noStressSwitch    -> To not consider stress markers while using cmudict.wfst  

Something that was attempted unsuccessfully is sequence-to-sequence learning. We tried with learning on the phoneme sequence as well as word sequence. However, the network was unable to learn anything useful, most probably due to the severely limited training data. The relevant scripts are

phone_to_word/char2char_seq2seq.py and phone_to_word/phone2phone_seq2seq.py